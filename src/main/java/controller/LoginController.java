package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.User;
import dao.DisplayDao;
import dao.LoginDao;

@WebServlet("/loginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String name = request.getParameter("username");
		String pass = request.getParameter("password");
		User user = new User(name, pass);	
		HttpSession session = request.getSession();	
		User user2 = (User)session.getAttribute("key_session");
		if(user2 != null) {
			if(LoginDao.checkLogin(user2)) {
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Home.jsp");
				rd.forward(request, response);
			}	
		}
		else {
			if(LoginDao.checkLogin(user)) {
				session.setAttribute("key_session", user);	
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Home.jsp");
				rd.forward(request, response);
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Login.jsp");
				rd.forward(request, response);
			}
		}
		
		
	}

}
