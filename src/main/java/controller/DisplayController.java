package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.User;
import dao.DisplayDao;


@WebServlet("/displayController")
public class DisplayController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		List<User> list = DisplayDao.getAllUser();
		request.setAttribute("key1", list);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/Home.jsp");
		rd.forward(request, response);
	}

}
