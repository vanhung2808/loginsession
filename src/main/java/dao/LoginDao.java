package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.DBConnection;
import bean.User;

public class LoginDao {
	public static boolean checkLogin(User user) {
		Connection cn = DBConnection.createConnection();
		String sql = "select * from user where username = ? and password = ?";
		PreparedStatement cmd;
		boolean check = false;
		try {
			cmd = cn.prepareStatement(sql);
			cmd.setString(1, user.getUsername());
			cmd.setString(2, user.getPassword());
			ResultSet rs  = cmd.executeQuery();
			check = rs.next();
			cmd.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return check;
	}
}
