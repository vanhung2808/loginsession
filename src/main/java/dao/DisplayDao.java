package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.User;
import database.DBConnection;

public class DisplayDao {
	public static List<User> getAllUser() {
		List<User> list = new ArrayList<User>();
		list = null;
		Connection cn = DBConnection.createConnection();
		String url = "select * from user";		
		try {
			PreparedStatement cmd = cn.prepareStatement(url);
			ResultSet rs = cmd.executeQuery();
			while(rs.next()) {
				String name = rs.getString("username");
				String pass = rs.getString("password");
				User user = new User(name, pass);
				list.add(user);
			}
			rs.close();
			cmd.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
