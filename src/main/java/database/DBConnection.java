package database;

import java.sql.*;

public class DBConnection {
	public static Connection createConnection()  {
		Connection conn = null; 
		String url = "jdbc:mysql://localhost:3306/example";
		String username = "root";
		String password = "1234";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection(url,username, password);
		} catch (ClassNotFoundException  | SQLException e1) {
			// TODO: handle exception
		}
		
		 return conn;
	 }
}
